<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailCertificate extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $certificate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $certificate)
    {
        $this->email = $email;
        $this->certificate = $certificate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Certificate Details')
            ->view('mail.certificate')
            ->attach($this->certificate, [
                'as' => 'certificate-of-completion.pdf',
                'mime' => 'application/pdf',
            ]);
    }
}
