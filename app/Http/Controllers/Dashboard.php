<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use Auth;

class Dashboard extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $user = $this->getUser();

        JavaScript::put([
            'user' => array(
                'id' => $user->id,
                'role' => $user->role,
                'name' => $user->name,
                'active' => $user->active
            )
        ]);

        return view('admin/dashboard');
    }

    private function getUser() {
        return Auth::user();
    }
}
