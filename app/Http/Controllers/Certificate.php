<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailCertificate;

class Certificate extends Controller
{
    function sendCertificate(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'certificate' => 'required|file'
        ]);

        if ($validator->fails() || $request->certificate->extension() !== "pdf") {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong, please check all details and try again'
            ]);
        }

        $email = $request->email;
        $certificate = $request->certificate;

        Mail::to(array(
            'email' => $email
        ))->send(new MailCertificate($email, $certificate));

        return array(
            "success" => true,
            "email" => $email,
            "certificate" => $request->certificate->extension()
        );
    }
}
