<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegistered;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    private function getErrorMessage($e) {
        return array(
            'succes' => false,
            'message' => $e->getMessage()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email',
                'role' => [
                    'required',
                    Rule::in(['student', 'teacher'])
                ]
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => 'Something went wrong, please check all details and try again'
                ]);
            }

            $password = str_random(12);
            $user_data = array(
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($password),
                'role' => $request->role
            );

            $user = User::create($user_data);
            Mail::to($user)->send(new UserRegistered($user, $password));

            return response()->json([
                'success' => true
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            if (trim($request->name)) {
                $user->name = $request->name;
            }

            if (trim($request->role)) {
                $user->role = $request->role;
            }

            $active = $request->active == 'true' ? 1 : 0;
            $user->active = $active;

            $user->save();

            return response()->json([
                'success' => true,
                'user' => $user,
                'active' => $active,
                'req_active' => $request->active
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (Auth::check()) {
                if (Auth::user()->role !== 'admin') {
                    throw new \Exception('Unauthorized access');
                }

                if (Auth::user()->id == $id) {
                    throw new \Exception('Attempting to delete own user');
                }
            }
            
            $user = User::findOrFail($id);

            $user->delete();

            return response()->json([
                'success' => true
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    public function getAll()
    {
        if (Auth::check()) {
            if (Auth::user()->role === 'admin') {
                try {
                    $users = User::orderBy('created_at', 'desc')->get();

                    return response()->json([
                        'success' => true,
                        'users' => $users
                    ]);
                }
                catch (\Exception $e) {
                    return response()->json($this->getErrorMessage($e));
                }
            }
        }
        

        return response()->json([
            'success' => false,
            'message' => 'Unauthorized'
        ]);
    }

    public function getById($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }
}
