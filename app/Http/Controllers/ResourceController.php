<?php

namespace App\Http\Controllers;

use App\Resource as Resource;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Storage;

class ResourceController extends Controller
{
    private $uploadPath = 'web/uploads/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getErrorMessage($e) {
        return array(
            'succes' => false,
            'message' => $e->getMessage()
        );
    }

    private function ensureAdmin() {
        if (Auth::check()) {
            if (Auth::user()->role === 'admin') {
                return true;
            }
        }

        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($this->ensureAdmin() == false) {
                throw new \Exception('Unauthorized access');
            }

            $file = $request->file('download_path');
            $filename = md5(microtime()) . '-' . $file->getClientOriginalName();
    
            // $path = $request->file('download_path')->storeAs('resources', $filename);
            // $path = Storage::put($filename, $request->file('download_path'));

            $file->move($this->uploadPath, $filename);
            
            $resource = new Resource;
            $resource->title = $request->title;
            $resource->available_to = $request->available_to;
            $resource->download_path = $filename;

            $resource->save();

            return response()->json([
                'success' => true,
                'resource' => $resource
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if ($this->ensureAdmin() == false) {
                throw new \Exception('Unauthorized access');
            }

            $resource = Resource::findOrFail($id);

            if (trim($request->title)) {
                $resource->title = $request->title;
            }

            if (trim($request->available_to)) {
                $resource->available_to = $request->available_to;
            }

            if ($request->file('download_path')) {
                $file = $request->file('download_path');
                $filename = md5(microtime()) . '-' . $file->getClientOriginalName();
                $destination = $this->uploadPath;

                // $path = $request->file('download_path')->storeAs('resources', $filename);

                // $resource->download_path = $path;

                $file->move($destination, $filename);
                $resource->download_path = $filename;
            }

            $resource->save();

            return response()->json([
                'success' => true,
                'resource' => $resource
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if ($this->ensureAdmin() == false) {
                throw new \Exception('Unauthorized access');
            }
            
            $resource = Resource::findOrFail($id);

            $resource->delete();

            return response()->json([
                'success' => true
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }

    /**
     * Get all the resources - intended for Admins only
     * 
     */
    public function getAll()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 'admin' || Auth::user()->role == 'teacher') {
                try {
                    $resources = Resource::orderBy('created_at', 'desc')->get();

                    return response()->json([
                        'success' => true,
                        'resources' => $resources
                    ]);
                }
                catch (\Exception $e) {
                    return response()->json($this->getErrorMessage($e));
                }
            }
            else {
                try {
                    $resources = Resource::orderBy('created_at', 'desc')
                        ->where('available_to', 'student')
                        ->get();

                    return response()->json([
                        'success' => true,
                        'resources' => $resources
                    ]);
                }
                catch (\Exception $e) {
                    return response()->json($this->getErrorMessage($e));
                }
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Unauthorized'
        ]);
    }

    public function getById($id)
    {
        try {
            $resource = Resource::findOrFail($id);

            return response()->json([
                'success' => true,
                'resource' => $resource
            ]);
        }
        catch (\Exception $e) {
            return response()->json($this->getErrorMessage($e));
        }
    }
}
