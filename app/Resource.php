<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = "resources";
    protected $fillable = [
        "title", "available_to", "download_path"
    ];
}
