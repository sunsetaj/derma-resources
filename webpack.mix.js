const mix = require('laravel-mix');
const Jarvis = require("webpack-jarvis");

if (!mix.inProduction()) {
    if (process.env.MIX_BROWSER_SYNC) {
        mix.browserSync(process.env.MIX_BROWSER_SYNC);
    }
    
    mix.webpackConfig({
        plugins: [
            new Jarvis({
                port: 1337
            })
        ]
    });
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (mix.inProduction()) {
   mix.options({
       uglify: {
           uglifyOptions: {
               compress: {
                   drop_console: true,
                   dead_code: true
               },
               ie8: false
           },
           extractComments: true
       }
   });
}

mix.version();
mix.sourceMaps();

mix.react('resources/js/pages/dashboard', 'public/js')
   .extract([
      'react',
      'axios',
      'react-router-dom'
   ])
   
mix.sass('resources/sass/app.scss', 'public/css');
