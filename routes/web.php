<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('dashboard');
    }
    return view('auth/login');
});

Auth::routes([
    'register' => false
]);

Route::get('/dashboard', 'Dashboard@index')->name('dashboard');
Route::get('/dashboard/{all?}', 'Dashboard@index')->name('dashboard')->where('all', '.*');

Route::post('/resource/create', 'ResourceController@store');
Route::post('/resource/edit/{id}', 'ResourceController@update');
Route::get('/resources.getall', 'ResourceController@getAll');
Route::get('/resource/get/{id}', 'ResourceController@getById');
Route::delete('/resource/delete/{id}', 'ResourceController@destroy');

Route::post('/user/create', 'UserController@store');
Route::post('/user/edit/{id}', 'UserController@update');
Route::get('/users.getall', 'UserController@getAll');
Route::get('/user/get/{id}', 'UserController@getById');
Route::delete('/user/delete/{id}', 'UserController@destroy');