export const validateValues = (values, ignore = null) => {
    for (let key in values) {
        if (ignore !== null && typeof ignore === 'object') {
            if (ignore.indexOf(key) !== -1){
                continue
            }
        }

        let val = values[key]

        if (!val.trim()) {
            return false
        }
    }

    return true
}

export const constructFormData = (data) => {
    let formdata = new FormData()

    for (let key in data) {
        formdata.append(key, data[key])
    }

    return formdata
}