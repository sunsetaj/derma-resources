import React from 'react'
import axios from 'axios'

import CreationLayout from '../../elements/Admin/CreationLayout'
import {
    FormActions,
    FormGroup,
    TextInput,
    Label,
    SelectInput,
    Checkbox
} from '../../elements/Forms/Forms'
import {
    validateValues,
    constructFormData
} from '../../plugins/form'

export default class UserForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            role: '',
            active: true,
            processing: false,
            loading: false
        }
    }

    componentDidMount() {
        let {
            match
        } = this.props.routerProps

        if (match.params.type === 'edit') {
            let id = match.params.id
            if (id) {
                this.setState({
                    loading: true
                })

                axios.get(`/user/get/${id}`)
                    .then(res => {
                        let data = res.data
                        if (data.success) {
                            let user = data.user
                            this.setState({
                                name: user.name,
                                role: user.role,
                                email: user.email,
                                active: !!parseInt(user.active)
                            })
                        }
                        else {
                            alert(data.message)
                        }
                    })
                    .catch(e => {
                        console.error(e)
                    })
                    .finally(() => {
                        this.setState({
                            loading: false
                        })
                    })
            }
        }
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.processing) return false

        let { match } = this.props.routerProps,
            id = match.params.id,
            type = match.params.type,
            url = ''
        
        if (type !== 'edit') {
            let valid = validateValues(this.state, ['processing', 'loading', 'active']);
            if (!valid) {
                alert('Please fill in all the fields.')
                return false
            }
        }

        let formdata = constructFormData({
            name: this.state.name,
            email: this.state.email,
            role: this.state.role,
            active: this.state.active
        })

        this.setState({
            processing: true
        })

        switch (type) {
            case 'create':
                // this.saveUser(formdata)
                url = '/user/create'
                break;
            case 'edit':
                // this.editUser(formdata, id)
                url = `/user/edit/${id}`
                break;
            default:
                alert('Unknown URL Pattern');
                break;
        }

        if (!url.trim()) return false

        axios.post(url, formdata)
            .then(({data}) => {
                if (data.success) {
                    switch (type) {
                        case 'create':
                            this.saveUserSuccess()
                            break;
                        case 'edit':
                            this.editUserSuccess(data)
                            break;
                        default:
                            alert('Success')
                            break;
                    }
                }
                else {
                    alert(data.message)
                }
            })
            .catch(e => {
                console.error(e)
            })
            .finally(() => {
                this.setState({
                    processing: false
                })
            })
    }

    editUserSuccess(data) {
        this.setState({
            name: data.user.name,
            role: data.user.role
        }, () => {
            alert('User Updated');
        })
    }

    saveUserSuccess() {
        this.setState({
            name: '',
            email: '',
            role: ''
        }, () => {
            alert('User Created')
        })
    }

    onChange(e) {
        if (e.target.type !== 'checkbox') {
            this.setState({
                [e.target.name]: e.target.value
            })
        }
        else {
            this.setState({
                [e.target.name]: !this.state[e.target.name]
            })
        }
    }

    render() {
        let {
            match
        } = this.props.routerProps
        
        let params = match.params

        let crumbs = [
            {
                title: 'Dashboard',
                active: false,
                to: '/dashboard'
            },
            {
                title: `${params.type} User`,
                active: true
            }
        ]

        return (
            <CreationLayout
                crumbs={crumbs}
                title={`${params.type} User`} 
                loading={this.state.loading}
            >
                <form onSubmit={(e) => this.onSubmit(e)} encType="multipart/form-data">
                    <FormGroup>
                        <Label htmlFor="name" text="Name" />
                        <TextInput
                            name="name"
                            onChange={(e) => this.onChange(e)}
                            value={this.state.name}
                        />
                    </FormGroup>
                    {
                        params.type !== 'create' ? (
                            ''
                        ) : (
                            <FormGroup>
                                <Label htmlFor="email" text="Email" />
                                <TextInput
                                    type="email"
                                    name="email"
                                    onChange={(e) => this.onChange(e)}
                                    value={this.state.email}
                                />
                            </FormGroup>
                        )
                    }
                    <FormGroup>
                        <Label htmlFor="role" text="Role" />
                        <SelectInput
                            name="role"
                            onChange={(e) => this.onChange(e)}
                            value={this.state.role}
                            options={[
                                { value: 'student', text: 'Student' },
                                { value: 'teacher', text: 'Teacher' }
                            ]}
                        />
                    </FormGroup>
                    {
                        params.type !== 'create' ? (
                            <FormGroup classNames="form-check">
                                <Checkbox
                                    checked={this.state.active}
                                    name="active"
                                    onChange={(e) => this.onChange(e)}
                                    label="Change user Active state"
                                />
                            </FormGroup>
                        ) : ('')
                    }

                    <FormActions loading={this.state.processing} />
                </form>
            </CreationLayout>
        )
    }
}