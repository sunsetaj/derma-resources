import React from 'react'
import axios from 'axios'

import CreationLayout from '../../elements/Admin/CreationLayout'

import {
    FormActions,
    FormGroup,
    TextInput,
    Label,
    SelectInput,
    FileInput
} from '../../elements/Forms/Forms'

import {
    validateValues,
    constructFormData
} from '../../plugins/form'

export default class ResourceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            available_to: '',
            download_path: '',
            loading: false,
            display_title: '',
            processing: false
        }

        this.fileUpload = null
    }

    componentDidMount() {
        let match = this.props.routerProps.match

        if (match.params.type === 'edit') {
            let id = match.params.id
            if (id) {
                this.setState({
                    loading: true
                })
    
                axios.get(`/resource/get/${id}`)
                    .then(({data}) => {
                        if (data.success) {
                            let resource = data.resource
                            this.setState({
                                title: resource['title'],
                                available_to: resource['available_to'],
                                display_title: resource['title']
                            })
                        }
                    })
                    .catch(e => {
                        console.error(e)
                    })
                    .finally(() => {
                        this.setState({
                            loading: false
                        })
                    })
            }
        }
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.processing) return false

        let { match } = this.props.routerProps,
            id = match.params.id,
            type = match.params.type,
            url = ''
        
        if (type !== 'edit') {
            let valid = validateValues(this.state, ['loading', 'display_title', 'processing'])
            if (!valid) {
                alert('Please fill in all the fields')
                return false
            }
        }

        this.setState({ processing: true })

        let formdata = constructFormData({
            title: this.state.title,
            available_to: this.state.available_to
        })
        
        if (this.fileUpload.files.length > 0) {
            formdata.append('download_path', this.fileUpload.files[0])
        }
        else {
            if (type === 'create') {
                alert('Please Upload a File');
            }
        }

        switch (type) {
            case 'create':
                url = '/resource/create'
                break;
            case 'edit':
                url = `/resource/edit/${id}`
                break;
            default:
                alert('Unknown URL Pattern');
                break;
        }

        if (!url.trim()) return false

        axios.post(url, formdata, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(({data}) => {
            if (data.success) {
                switch (type) {
                    case 'create':
                        this.saveResourceSuccess()
                        break;
                    case 'edit':
                        this.editResourceSuccess(data)
                        break;
                    default:
                        alert('Success')
                        break;
                }
            }
            else {
                alert(data.message)
            }
        })
        .catch(e => {
            console.error(e)
        })
        .finally(() => {
            this.setState({ processing: false })
        })
    }

    editResourceSuccess(data) {
        this.setState({
            title: data.resource['title'],
            available_to: data.resource['available_to'],
            display_title: data.resource['title']
        }, () => {
            alert('Resource Updated');
        })
    }

    saveResourceSuccess() {
        this.setState({
            title: '',
            available_to: '',
            download_path: ''
        }, () => {
            alert('Resource Created!');
        })
    }

    render() {
        const params = this.props.routerProps.match.params

        let form_title;
        if (params.type === 'edit') {
            form_title = `Edit "${ this.state.display_title }"`
        }
        else {
            form_title = `${params.type} resource`
        }

        let crumbs = [
            {
                title: 'Dashboard',
                active: false,
                to: '/dashboard'
            },
            {
                title: `${params.type} Resource`,
                active: true
            }
        ]

        return (
            <CreationLayout
                crumbs={crumbs}
                title={form_title}
                loading={this.state.loading}
            >
                <form onSubmit={(e) => this.onSubmit(e)} encType="multipart/form-data">
                    <FormGroup>
                        <Label 
                            htmlFor="title" 
                            text="Title"
                        />
                        <TextInput
                            name="title"
                            onChange={(e) => this.onChange(e)}
                            value={this.state.title}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            htmlFor="available_to"
                            text="Available To"
                        />
                        <SelectInput
                            name="available_to"
                            onChange={(e) => this.onChange(e)}
                            value={this.state.available_to}
                            options={[
                                { value: 'student', text: 'Students' },
                                { value: 'teacher', text: 'Teachers' }
                            ]}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            htmlFor="download_path"
                            text="Upload File"
                        />
                        <FileInput
                            type="file"
                            name="download_path"
                            onChange={(e) => this.onChange(e)}
                            value={this.state.download_path}
                            setRef={(ref) => {
                                this.fileUpload = ref
                            }}
                            accept=".pdf"
                        />
                    </FormGroup>
                    
                    <FormActions loading={this.state.processing} />
                </form>
            </CreationLayout>
        )
    }
}