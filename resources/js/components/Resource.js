import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import Card from './Card'

export default class Resource extends React.PureComponent {

    deleteResource(e, id) {
        e.preventDefault();

        if (confirm('Are you sure?')) {
            axios.delete(`/resource/delete/${id}`)
                .then(res => {
                    let data = res.data
                    if (data.success) {
                        alert('Resource deleted. Press "Ok" to reload page')
                        location.reload(true);
                    }
                    else {
                        alert(data.message)
                    }
                })
                .catch(e => {
                    console.error(e)
                })
        }
    }

    render() {
        const { data } = this.props
        const user = window.user;

        return (
            <Card title={data.title}>
                {
                    user.role === 'admin' ? (
                        <Fragment>
                            <p>Available To: <strong>{ data.available_to }</strong></p>
                            <Link to={`/dashboard/resource/edit/${data.id}`}>Edit</Link>
                            <a href="#" onClick={(e) => this.deleteResource(e, data.id)} className="text-danger" style={{ marginLeft: '20px' }}>Delete</a>
                        </Fragment>
                    ) : (
                        <a href="#" className="btn btn-primary" onClick={(e) => {
                            e.preventDefault()
                            this.props.renderViewer(data.download_path)
                        }}>View</a>
                    )
                }
            </Card>
        )
    }
}