import React, { PureComponent } from 'react'

export default class Card extends PureComponent {
    render() {
        return (
            <div className={`card ${this.props.className || ''}`}>
                <div className="card-body">
                    <h5 className="card-title">{ this.props.title || '' }</h5>
                    { this.props.children }
                </div>
            </div>
        )
    }
}