import React, { Fragment } from 'react'
import axios from 'axios'

import Resource from '../Resource'

export default class CommonDashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            resources: null,
            loading: true,
            url: null
        }
    }

    componentDidMount() {
        this.fetchAllResources();
    }

    fetchAllResources() {
        this.setState({
            loading: true
        })

        axios.get('/resources.getall')
            .then(res => {
                let data = res.data
                if (data.success) {
                    this.setState({
                        loading: false,
                        resources: data.resources
                    })
                }
                else {
                    this.setState({
                        loading: false
                    }, () => {
                        alert(data.message)
                    })
                }
            })
            .catch(e => {
                console.error(e)
                this.setState({
                    loading: false
                })
            })
    }

    renderResources(resources) {
        if (resources && resources.length > 0) {
            return resources.map((resource, i) => {
                return <div className="mb-2" key={resource.id}><Resource data={resource} renderViewer={(url) => this.renderViewer(url)} /></div>
            })
        }
        else {
            return (
                <div className="not-found">
                    <h5>No Resources found</h5>
                    {
                        this.state.resources.length < 0 || this.state.resources === null ? (
                            <a href="#" onClick={(e) => {
                                e.preventDefault();
                                this.fetchAllResources();
                            }} className="btn btn-secondary">Refresh</a>       
                        ) : (
                            ''
                        )
                    }
                </div>
            )
        }
    }

    renderViewer(url) {
        this.setState({
            url: url
        })
    }

    render() {
        let {
            loading,
            resources,
            url
        } = this.state

        return (
            <div className="row">
                <div className="col-md-12">
                    <h3>Welcome {window.user.name}</h3>
                    <hr/>
                </div>
                {
                    window.user.active ? (
                        <Fragment>
                            <div className="col-md-6">
                                {
                                    loading ? (
                                        <p className="lead">Loading...</p>
                                    ) : (
                                        this.renderResources(resources)
                                    )
                                }
                            </div>
                            <div className="col-md-6">
                                <h4>Document Viewer</h4>
                                <hr/>
                                {
                                    url ? (
                                        <iframe src={`/web/viewer.html?file=uploads/${url}`} style={{
                                            height: '500px',
                                            width: '100%'
                                        }} />
                                    ) : (
                                        <p className="lead">Select a Resource to view</p>
                                    )
                                }
                            </div>
                        </Fragment>
                    ) : (
                        <div className="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                Your account has been suspended. Get in touch with Dermaplane Pro Australia to resolve this issue.
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }
}