import React, { Fragment } from 'react'

import AllResources from './Index/AllResources'
import AllUsers from './Index/AllUsers'

export default class DashboardIndex extends React.PureComponent {
    render() {
        return (
            <Fragment>
                <div className="row">
                    <div className="col-md 12">
                        <h3>Dashboard</h3>
                        <hr/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <AllResources />
                    </div>
                    <div className="col-md-6">
                        <AllUsers />
                    </div>
                </div>
            </Fragment>
        )
    }
}