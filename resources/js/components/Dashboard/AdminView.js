import React, { Fragment } from 'react'
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'

import ResourceForm from '../Forms/ResourceForm'
import UserForm from '../Forms/UserForm'
import DashboardIndex from './DashboadIndex'

export default class AdminView extends React.Component {
    render() {
        return (
            <Router>
                <Fragment>
                    <Route path="/dashboard" exact component={DashboardIndex} />
                    <Route path="/dashboard/resource/:type/:id?" render={(routerProps) => {
                        return <ResourceForm routerProps={routerProps} />
                    }} />
                    <Route path="/dashboard/user/:type/:id?" render={(routerProps) => {
                        return <UserForm routerProps={routerProps} />
                    }} />
                </Fragment>
            </Router>
        )
    }
}