import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import Card from '../../Card'
import User from '../../User'

import {
    TextInput,
    SelectInput
} from '../../../elements/Forms/Forms'

export default class AllUsers extends Component {
    constructor() {
        super()
        this.state = {
            loading_users: true,
            users: null,
            search_term: '',
            role: ''
        }
    }

    componentDidMount() {
        this.fetchAllUsers();
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    fetchAllUsers() {
        this.setState({
            loading_users: true,
            users: null
        });

        axios.get('/users.getall')
            .then(res => {
                let data = res.data
                if (data.success) {
                    this.setState({
                        loading_users: false,
                        users: data.users
                    })
                }
                else {
                    this.setState({
                        loading_users: false
                    }, () => {
                        alert(data.message);
                    })
                }
            })
            .catch(e => {
                console.error(e)
                this.setState({
                    loading_users: false
                })
            })
    }

    renderUsers(users) {
        if (users && users.length > 0) {
            return users.map((user, i) => {
                return <div className="mb-2" key={user.id}><User data={user} /></div>
            })
        }
        else {
            return (
                <div className="not-found">
                    <h5>No Users found</h5>
                    {
                        this.state.users.length < 0 || this.state.users === null ? (
                            <a href="#" onClick={(e) => {
                                e.preventDefault();
                                this.fetchAllUsers();
                            }} className="btn btn-secondary">Refresh</a>       
                        ) : (
                            ''
                        )
                    }
                </div>
            )
        }
    }

    clearFilter(e) {
        e.preventDefault();
        this.setState({
            search_term: '',
            role: ''
        })
    }

    render() {
        const {
            loading_users,
            users,
            search_term,
            role
        } = this.state

        let users_to_show = users

        if (search_term.trim() !== '' || role.trim() !== '') {
            users_to_show = []
            users.forEach(item => {
                if (search_term.trim() !== '' && role.trim() !== '') {
                    if (item.role === role && item.name.toLowerCase().includes(search_term.toLowerCase())) {
                        users_to_show.push(item)
                    }
                }
                else if (search_term.trim() !== '') {
                    if (item.name.toLowerCase().includes(search_term.toLowerCase())) {
                        users_to_show.push(item)
                    }
                }
                else if (role.trim() !== '') {
                    if (item.role === role) {
                        users_to_show.push(item)
                    }
                }
            })
        }

        return (
            <Fragment>
                <Card title="Create User">
                    <p>Create a user to access this information</p>
                    <Link to="/dashboard/user/create" className="card-link">Add User</Link>
                </Card>

                <h4 style={{marginTop: '30px'}}>All Users</h4>
                <hr/>
                <div className="input-group mb-3">
                    <TextInput name="search_term" placeholder="Search Users" onChange={(e) => this.onChange(e)} value={search_term} />
                    <SelectInput
                        classNames="custom-select"
                        name="role"
                        value={role}
                        onChange={(e) => this.onChange(e)}
                        options={[
                            { value: 'student', text: 'Students' },
                            { value: 'teacher', text: 'Teachers' },
                            { value: 'admin', text: 'Admins' }
                        ]}
                    />
                    <div className="input-group-append">
                        <a className="btn btn-outline-secondary" onClick={(e) => this.clearFilter(e)}>Clear</a>
                    </div>
                </div>
                {
                    loading_users ? (
                        <p className="lead">Loading...</p>
                    ) : (
                        this.renderUsers(users_to_show)
                    )
                }
            </Fragment>
        )
    }
}