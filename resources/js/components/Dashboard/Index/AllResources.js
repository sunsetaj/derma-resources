import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import Resource from '../../Resource'
import Card from '../../Card'

import {
    TextInput
} from '../../../elements/Forms/Forms'

export default class AllResources extends Component {

    constructor() {
        super();
        this.state = {
            loading_resources: true,
            resources: null,
            search_term: ''
        }
    }

    componentDidMount() {
        this.fetchAllResources();
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    fetchAllResources() {
        this.setState({
            loading_resources: true,
            resources: null
        });

        axios.get('/resources.getall')
            .then(res => {
                let data = res.data
                if (data.success) {
                    this.setState({
                        loading_resources: false,
                        resources: data.resources
                    })
                }
                else {
                    this.setState({
                        loading_resources: false
                    }, () => {
                        alert(data.message);
                    })
                }
            })
            .catch(e => {
                console.error(e)
                this.setState({
                    loading_resources: false
                })
            })
    }

    renderResources(resources) {
        if (resources && resources.length > 0) {
            return resources.map((resource, i) => {
                return <div className="mb-2" key={resource.id}><Resource data={resource} /></div>
            })
        }
        else {
            return (
                <div className="not-found">
                    <h5>No Resources found</h5>
                    {
                        this.state.resources.length < 0 || this.state.resources === null ? (
                            <a href="#" onClick={(e) => {
                                e.preventDefault();
                                this.fetchAllResources();
                            }} className="btn btn-secondary">Refresh</a>       
                        ) : (
                            ''
                        )
                    }
                </div>
            )
        }
    }

    clearFilter(e) {
        e.preventDefault();
        this.setState({
            search_term: ''
        })
    }

    render() {
        const {
            loading_resources,
            resources,
            search_term
        } = this.state

        let resources_to_show = resources

        if (search_term.trim() !== '') {
            resources_to_show = [];
            
            resources.forEach(item => {
                if (item.title.toLowerCase().includes(search_term.toLowerCase())) {
                    resources_to_show.push(item)
                }
            })
        }

        return (
            <Fragment>
                <Card title="Create Resource">
                    <p>Resources are student or teacher documents</p>
                    <Link to="/dashboard/resource/create" className="card-link">Add Resource</Link>
                </Card>
                
                <h4 style={{marginTop: '30px'}}>All Resources</h4>
                <hr/>
                <div className="input-group mb-3">
                    <TextInput
                        name="search_term" 
                        placeholder="Search Resources"
                        onChange={(e) => this.onChange(e)}
                        value={this.state.search_term}
                    />
                    <div className="input-group-append">
                        <a className="btn btn-outline-secondary" onClick={(e) => this.clearFilter(e)}>Clear</a>
                    </div>
                </div>
                {
                    loading_resources ? (
                        <p className="lead">Loading...</p>
                    ) : (
                        this.renderResources(resources_to_show)
                    )
                }
            </Fragment>
        )
    }
}