import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

export default class User extends React.PureComponent {

    deleteUser(e, id) {
        e.preventDefault();

        if (confirm('Are you sure?')) {
            axios.delete(`/user/delete/${id}`)
                .then(res => {
                    let data = res.data
                    if (data.success) {
                        alert('User deleted. Press "Ok" to reload page')
                        location.reload(true);
                    }
                    else {
                        alert(data.message)
                    }
                })
                .catch(e => {
                    console.error(e)
                })
        }
    }

    render() {
        const { data } = this.props
        const user = window.user;

        const role = data.role
        const active = data.active
        let style = '';

        switch (role) {
            case 'admin':
            style = 'badge-primary'
                break;
            case 'student':
                style = 'badge-success'
                break;
            case 'teacher':
                style = 'badge-info'
                break;
            default:
                style = 'badge-secondary'
                break;
        }

        return (
            <div className={`card`}>
                <div className="card-body">
                    <h5 className="card-title m-0">
                        { data.name }&nbsp;
                        <span className={`badge ${style}`}>{role}</span>&nbsp;
                        <span className={`badge ${active ? 'badge-primary' : 'badge-danger'}`}>{active ? 'active' : 'inactive'}</span>
                    </h5>
                    {
                        user.role === 'admin' && data.role !== 'admin' ? (
                            <Fragment>
                                <Link to={`/dashboard/user/edit/${data.id}`}>Edit</Link>
                                <a href="#" onClick={(e) => this.deleteUser(e, data.id)} className="text-danger" style={{ marginLeft: '20px' }}>Delete</a>
                            </Fragment>
                        ) : (
                            ''
                        )
                    }
                </div>
            </div>
        )
    }
}