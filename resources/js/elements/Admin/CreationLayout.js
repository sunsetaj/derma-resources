import React from 'react'
import Breadcrumbs from '../Breadcrumbs'
import Loading from '../Loading'

const CreationLayout = ({
    crumbs,
    title,
    loading,
    children
}) => (
    <div className="row">
        <div className="col-md-12">
            <Breadcrumbs crumbs={crumbs} />
        </div>
        <div className="col-md-12">
            <h3 className="text-capitalize">{title}</h3>
            <hr/>
            <div className="creation-body">
                { loading ? <Loading /> : children }
            </div>
        </div>
    </div>
)

export default CreationLayout