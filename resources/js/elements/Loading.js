import React from 'react'

const Loading = () => <p className="lead">Loading...</p>

export default Loading