import React from 'react'

export const TextInput = ({
    value,
    onChange,
    type,
    name,
    classNames,
    placeholder
}) => (
    <input
        type={type || 'text'}
        className={`form-control ${classNames || ''}`}
        name={name}
        onChange={(e) => onChange(e)}
        value={value}
        id={name}
        placeholder={placeholder || ''}
    />
)