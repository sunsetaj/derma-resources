import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'

export const FormActions = ({ 
    loading 
}) => (
    <Fragment>
        <hr/>
        {
            loading ? (
                <button disabled="disabled" className="btn btn-primary">Processing</button>
            ) : (
                <Fragment>
                    <button type="submit" className="btn btn-primary">Submit</button>
                    <Link className="btn btn-link" to="/dashboard">Cancel</Link>
                </Fragment>
            )
        }
    </Fragment>
)