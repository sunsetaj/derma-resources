import React, { Fragment } from 'react'

import { Label } from './Label'

export const Checkbox = ({
    checked,
    onChange,
    name,
    classNames,
    label
}) => (
    <Fragment>
        <input
            type="checkbox"
            className={`form-check-input ${classNames || ''}`}
            name={name}
            id={name}
            onChange={(e) => onChange(e)}
            checked={checked}
        />
        <Label htmlFor={name} text={label} />
    </Fragment>
)