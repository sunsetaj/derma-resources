import React from 'react'

export const SelectInput = ({
    value,
    onChange,
    name,
    classNames,
    options
}) => (
    <select
        className={`form-control ${classNames || ''}`}
        name={name}
        onChange={(e) => onChange(e)}
        value={value}
        id={name}
    >
        <option value="">Select Option</option>
        {
            options.map((option, i) => (
                <option value={option.value} key={i}>{option.text}</option>
            ))
        }
    </select>
)