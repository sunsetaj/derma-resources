import React from 'react'

export const Label = ({
    htmlFor,
    text,
    children
}) => (
    <label
        htmlFor={htmlFor}
    >
        { text ? text : '' }
        { children ? children : '' }
    </label>
)