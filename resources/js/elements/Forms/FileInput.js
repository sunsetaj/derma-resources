import React from 'react'

export const FileInput = ({
    value,
    onChange,
    name,
    classNames,
    setRef,
    accept
}) => (
    <input
        type='file'
        className={`form-control-file ${classNames || ''}`}
        name={name}
        onChange={(e) => onChange(e)}
        value={value}
        id={name}
        ref={(ref) => setRef(ref)}
        accept={accept || ''}
    />
)