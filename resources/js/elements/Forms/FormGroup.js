import React from 'react'

export const FormGroup = ({ 
    classNames, 
    children 
}) => (
    <div className={`form-group ${classNames || ''}`}>{children || ''}</div>
)