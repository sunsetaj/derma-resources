import React from 'react'
import { Link } from 'react-router-dom'
import classNames from '../plugins/classNames'

const Breadcrumbs = ({ 
    crumbs 
}) => {

    const renderCrumbs = (crumbs) => {
        return crumbs.map((item, i) => {
            let classes = classNames([
                'breadcrumb-item',
                'text-capitalize',
                {
                    class: 'active',
                    condition: item.active === true
                }
            ])

            return (
                <li className={classes} key={i}>
                    { item.active ? item.title : <Link to={item.to}>{item.title}</Link> }
                </li>
            )
        })
    }

    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb">{renderCrumbs(crumbs)}</ol>
        </nav>
    )
}

export default Breadcrumbs
