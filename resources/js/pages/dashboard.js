import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'

import AdminView from '../components/Dashboard/AdminView'
import CommonDashboard from '../components/Dashboard/CommonDashboard'

// require('../bootstrap')

class Dashboard extends React.Component {

    constructor() {
        super()
        this.state = {
            user: window.user
        }
    }

    render() {
        return (
            <Fragment>
                {
                    this.state.user.role !== 'admin' ? (
                        <CommonDashboard />
                    ) : (
                        <AdminView />
                    )
                }
            </Fragment>
        )
    }
}

if (document.getElementById('app__dashboard')) {
    ReactDOM.render(<Dashboard/>, document.getElementById('app__dashboard'))
}